<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParsedUri extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'scheme',
        'host',
        'port',
        'user',
        'pass',
        'path',
        'query',
    ];
}
