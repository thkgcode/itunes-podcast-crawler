<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feed extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['itunes_id'];

    public function rawData(): HasOne
    {
        return $this->hasOne(FeedRawData::class);
    }

    public function feedUri(): BelongsTo
    {
        return $this->belongsTo(Uri::class, 'feed_uri_id');
    }
}
